package com.luv2code.aopdemo.dao;

import com.luv2code.aopdemo.Account;
import org.springframework.stereotype.Component;

@Component
public class AccountDAO {

    private String name;
    private String serviceCode;


    public void addAccount(Account theAccount, boolean vipFlag){

        System.out.println(getClass() + ": Doing My DB Work: Adding an account");
    }

    public boolean doWork(){
        System.out.println(getClass() + ": doWork()");

        return true;
    }

    // Getters / Setters

    public String getName() {
        System.out.println(getClass() + ": in getName()");

        return name;
    }

    public void setName(String name) {
        System.out.println(getClass() + ": in SetName()");

        this.name = name;
    }

    public String getServiceCode() {
        System.out.println(getClass() + ": in getServiceCode()");

        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        System.out.println(getClass() + ": in setServiceCode()");

        this.serviceCode = serviceCode;
    }
}
